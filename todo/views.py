from django.shortcuts import render
from .models import Task

def task (request):
    tasks = Task.objects.order_by('date_task')
    return render(request, 'tasks.html', context={"tasks": tasks})