from django.db import models


class Task (models.Model):

    title = models.CharField(max_length=100)
    description = models.TextField()
    date_task = models.DateField(auto_now_add=True)
    time_task = models.TimeField(auto_now_add=True)
    deadline_task = models.DateField()
    STATUS_CHOICES = [
        ('01', 'Ожидает выполнения'),
        ('02', 'В процессе выполнения'),
        ('03', 'Выполнена'),

    ]
    name_status = models.CharField(choices=STATUS_CHOICES, max_length=100, verbose_name="Статус")
    PRIORITY_CHOICES = [
        ('01', 'Низкий'),
        ('02', 'Средний'),
        ('03', 'Высокий'),

    ]
    name_priority = models.CharField(choices=PRIORITY_CHOICES, max_length=100,verbose_name="Приоритет")

    def __str__(self):
        return self.name_priority

    def __str__(self):
        return self.name_status

    def __str__(self):
        return self.title


