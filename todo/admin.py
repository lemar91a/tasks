from django.contrib import admin
from todo.models import Task


class TaskAdmin (admin.ModelAdmin):
    list_display = ('id', 'title', 'status')


admin.site.register(Task)
